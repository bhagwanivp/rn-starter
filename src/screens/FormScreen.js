import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import InputField from "../components/InputField";

const FormScreen = () => {
	const [name, setName] = useState("");
	return (
		<View style={styles.wrapperView}>
			<InputField type='text' value={name} setValue={setName} />
		</View>
	);
};

const styles = StyleSheet.create({
	wrapperView: {
		marginVertical: 10,
	},
});

export default FormScreen;
