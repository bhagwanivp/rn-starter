import React, { useReducer } from "react";
import { Text, StyleSheet, View, Button } from "react-native";

const ColorBtns = ({ title, color, setColor }) => {
	const COLOR_CHANGE = 15;

	return (
		<View style={{ display: "flex", justifyContent: "space-between" }}>
			<Text>
				{title}: {color}
			</Text>
			<Button
				title='more'
				onPress={() => setColor(color + COLOR_CHANGE)}
			/>
			<Button
				title='less'
				onPress={() => setColor(color - COLOR_CHANGE)}
			/>
		</View>
	);
};

export const MakeColorScreen = () => {
	const reducer = (state, { type, payload }) => {
		const range = (n) => (n < 0 ? 0 : n > 255 ? 255 : n);

		switch (type) {
			case "set_r":
				return { ...state, r: range(payload) };
			case "set_g":
				return { ...state, g: range(payload) };
			case "set_b":
				return { ...state, b: range(payload) };
		}
	};

	const [state, dispatch] = useReducer(reducer, { r: 0, g: 0, b: 0 });

	return (
		<View>
			<Text style={styles.text}>Make Color Screen</Text>
			<ColorBtns
				title='Red'
				color={state.r}
				setColor={(val) => dispatch({ type: "set_r", payload: val })}
			/>
			<ColorBtns
				title='Green'
				color={state.g}
				setColor={(val) => dispatch({ type: "set_g", payload: val })}
			/>
			<ColorBtns
				title='Blue'
				color={state.b}
				setColor={(val) => dispatch({ type: "set_b", payload: val })}
			/>
			<View
				style={{
					...styles.colorBox,
					backgroundColor: `rgb(${state.r}, ${state.g}, ${state.b})`,
				}}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	btn: {},
	text: {},
	colorBox: {
		height: 100,
		width: 100,
	},
});

export default MakeColorScreen;
