import React, { useState } from "react";
import { Text, StyleSheet, View, Button } from "react-native";
import { FlatList } from "react-native-gesture-handler";

const randInt = (max) => Math.floor(Math.random() * max) + 1;

const randRGB = () => {
	const r = randInt(255);
	const b = randInt(255);
	const g = randInt(255);

	return `rgb(${r}, ${g}, ${b})`;
};

export const ColorScreen = () => {
	const [colors, setColors] = useState([]);
	const addColor = () => {
		const newColor = randRGB();
		console.log("add color click", newColor);
		setColors((colors) => [...colors, newColor]);
	};
	return (
		<View>
			<Text style={styles.text}>Color Screen</Text>
			<Button
				title='Add random color'
				onPress={addColor}
				style={styles.btn}
			/>
			<FlatList
				keyExtractor={(_, i) => String(i)}
				data={colors}
				renderItem={({ item }) => (
					<View
						style={{
							...styles.colorBox,
							backgroundColor: item,
						}}
					/>
				)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	btn: {},
	text: {},
	colorBox: {
		height: 100,
		width: 100,
	},
});

export default ColorScreen;
