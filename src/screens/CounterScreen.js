import React, { useState } from "react";
import { Text, StyleSheet, View, Button } from "react-native";

export const CounterScreen = () => {
	const [count, setCount] = useState(0);

	return (
		<View>
			<Button
				title='+1'
				style={styles.btn}
				onPress={() => setCount(count + 1)}
			/>

			<Text style={styles.text}>Current Count: </Text>
			<Text style={styles.count}>{count}</Text>

			<Button
				title='-1'
				style={styles.btn}
				onPress={() => setCount(count - 1)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	btn: {
		marginVertical: 10,
	},
	text: {
		textAlign: "center",
		fontSize: 30,
	},
	count: {
		textAlign: "center",
		fontSize: 50,
	},
});

export default CounterScreen;
