import React from "react";
import { Text, StyleSheet, View, Button } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const HomeScreen = ({ navigation }) => {
	return (
		<View>
			<Text style={styles.heading}>
				Getting started with React Native!
			</Text>
			<Button
				title='Components demo Button'
				onPress={() => navigation.navigate("Component")}
			/>
			<TouchableOpacity onPress={() => navigation.navigate("FlatList")}>
				<Text style={styles.touchable}>FlatList demo Touchable</Text>
			</TouchableOpacity>
			<Button
				title='Image demo Button'
				onPress={() => navigation.navigate("Image")}
			/>
			<TouchableOpacity onPress={() => navigation.navigate("Counter")}>
				<Text style={styles.touchable}>Counter app Touchable</Text>
			</TouchableOpacity>
			<Button
				title='Color Generator app Button'
				onPress={() => navigation.navigate("Color")}
			/>
			<TouchableOpacity onPress={() => navigation.navigate("MakeColor")}>
				<Text style={styles.touchable}>Make Color app Touchable</Text>
			</TouchableOpacity>
			<Button
				title='Form demo Button'
				onPress={() => navigation.navigate("Form")}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	heading: {
		fontSize: 45,
	},
	touchable: {
		textAlign: "center",
		padding: 10,
		fontSize: 30,
		fontWeight: "bold",
	},
});

export default HomeScreen;
