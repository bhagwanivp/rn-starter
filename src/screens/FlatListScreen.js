import React from "react";
import { Text, StyleSheet, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";

const friendsArr = Array.from(Array(10), (_, i) => ({
	name: "Friend #" + (i + 1),
	age: Math.floor(Math.random() * 50) + 1,
}));

const FlatListScreen = () => {
	return (
		<View>
			<FlatList
				keyExtractor={(friend) => friend.name}
				data={friendsArr}
				renderItem={({ item }) => (
					<Text style={styles.friendName}>
						{item.name}, {item.age}yrs
					</Text>
				)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	friendName: {
		marginVertical: 20,
		fontSize: 25,
	},
});

export default FlatListScreen;
