import React from "react";
import { Text, StyleSheet, View } from "react-native";

const ComponentScreen = (props) => {
	const name = "Vivek Bhagwani";
	const intro = (
		<Text style={styles.introduction}>Hi, my name is {name}</Text>
	);

	return <View>{intro}</View>;
};

const styles = StyleSheet.create({
	introduction: {
		fontSize: 20,
	},
});

export default ComponentScreen;
