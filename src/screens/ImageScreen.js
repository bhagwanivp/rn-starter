import React from "react";
import { View, StyleSheet } from "react-native";
import ImageCard from "../components/ImageCard";

export const ImageScreen = () => {
	const cardsArr = [
		{
			title: "Forest",
			imageUrl: require("../../assets/forest.jpg"),
			score: 9,
		},
		{
			title: "Beach",
			imageUrl: require("../../assets/beach.jpg"),
			score: 8,
		},
		{
			title: "Mountain",
			imageUrl: require("../../assets/mountain.jpg"),
			score: 10,
		},
	];
	return (
		<View style={styles.view}>
			<ImageCard {...cardsArr[0]} />
			<ImageCard {...cardsArr[1]} />
			<ImageCard {...cardsArr[2]} />
		</View>
	);
};

const styles = StyleSheet.create({
	view: {},
});

export default ImageScreen;
