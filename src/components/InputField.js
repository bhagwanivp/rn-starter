import React from "react";
import { TextInput, StyleSheet, View } from "react-native";

const InputField = ({ type, value, setValue, inputProps = {} }) => {
	return (
		<View>
			{type === "text" && (
				<TextInput
					value={value}
					onChangeText={setValue}
					style={styles.textInput}
					autoCapitalize='none'
					autoCorrect={false}
					autoCompleteType='off'
				/>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	textInput: {
		margin: 10,
		borderColor: "black",
		borderWidth: 1,
		borderStyle: "solid",
		fontSize: 24,
	},
});

export default InputField;
