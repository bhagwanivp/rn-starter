import React from "react";
import { Text, StyleSheet, View, Image } from "react-native";

export const ImageCard = ({ title, imageUrl, score }) => {
	return (
		<View>
			<Image source={imageUrl} />
			<Text style={styles.text}>Score-{score}</Text>
			<Text style={styles.text}>{title}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	text: {},
});

export default ImageCard;
