import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import HomeScreen from "./src/screens/HomeScreen";
import FlatListScreen from "./src/screens/FlatListScreen";
import ComponentScreen from "./src/screens/ComponentScreen";
import ImageScreen from "./src/screens/ImageScreen";
import CounterScreen from "./src/screens/CounterScreen";
import ColorScreen from "./src/screens/ColorScreen";
import MakeColorScreen from "./src/screens/MakeColorScreen";
import FormScreen from "./src/screens/FormScreen";

const navigator = createStackNavigator(
	{
		Home: HomeScreen,
		Component: ComponentScreen,
		FlatList: FlatListScreen,
		Image: ImageScreen,
		Counter: CounterScreen,
		Color: ColorScreen,
		MakeColor: MakeColorScreen,
		Form: FormScreen,
	},
	{
		initialRouteName: "Home",
		defaultNavigationOptions: {
			title: "App",
		},
	}
);

export default createAppContainer(navigator);
